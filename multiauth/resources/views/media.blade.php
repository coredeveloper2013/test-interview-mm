@extends('layouts.app')

@section('content')
<media-manager></media-manager>
<media-modal v-if="showMediaManager" @media-modal-close="showMediaManager = false">
    <media-manager
        :is-modal="true"
        :selected-event-name="selectedEventName"
        @media-modal-close="showMediaManager = false"
    >
    </media-manager>
</media-modal>
@endsection