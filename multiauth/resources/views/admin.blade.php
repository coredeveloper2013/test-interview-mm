@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="card">
                <div class="card-header">Admin Dashboard</div>

                <div class="card-body">
                   

                    @if (isset($message))
                        <p class="text-normal">
                            <strong>{{ Auth::user()->name }}</strong>, {{ $message }}
                        </p>
                    @else
                        @component('components.who')
                        @endcomponent
                    @endif

                    @if (Auth::user()->role == 'System Admin')
                        <a href="{{ route('create.admin') }}" class="btn btn-primary">
                            Create Admin
                        </a>
                    @endif

                    @if (Auth::user()->role == 'Admin')
                        <a href="{{ route('create.manager') }}" class="btn btn-primary">
                            Create Manager
                        </a>
                    @endif

                
                    
                    <div class="list-group" style="margin-top:20px;">
                        @if (Auth::user()->role == 'System Admin' && isset($admins))
                            <h4>Available Admins</h4>
                            @foreach ($admins as $admin)
                                <li class="list-group-item">
                                    Name : {{ $admin->name }} <br>
                                    Email : {{ $admin->email }}
                                </li>
                            @endforeach
                        @elseif (Auth::user()->role == 'Admin' && isset($managers))
                            <h4>Available Managers</h4>
                            @foreach ($managers as $manager)
                                <li class="list-group-item">
                                    Name : {{ $manager->name }} <br>
                                    Email : {{ $manager->email }}
                                </li>
                            @endforeach
                        @elseif(Auth::user()->role == 'Manager' && isset($customers))
                            <h4>Available Customers</h4>
                            @foreach ($customers as $customer)
                                <li class="list-group-item">
                                    Name : {{ $customer->name }} <br>
                                    Email : {{ $customer->email }}
                                </li>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection