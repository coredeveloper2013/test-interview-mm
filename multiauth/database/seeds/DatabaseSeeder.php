<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // seeding super admin with one data
        // DB::table('admins')->insert([
        //     'name' => 'SuperAdmin',
        //     'email' => 'super@admin.com',
        //     'password' => '123456',
        //     'role' => 'System Admin'
        // ]);

        $admin = new App\Admin;
        $admin->name = 'System Admin';
        $admin->password = Hash::make('123456');
        $admin->email = 'super@admin.com';
        $admin->role = 'System Admin';
        $admin->save();
    }
}
